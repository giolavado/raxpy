# RAXpy

RAXpy is a read-across tool based on structural, biological and metabolic similarities. It allows to calculate similarities between a target molecule and chemicals from a source dataset. Two datasets are included related to high-tier *in vivo* toxicological endpoints, i.e., human hepatotoxicity (ToxRef) and drug induced
liver injury (DILI). The predicted activity for the target is obtained using the activities of analogue(s) included in the similarity lists. Moreover, filters based on the presence of maximum common substructures (MCS) and common functional groups (FGs) between the target and the source compounds can be applied to narrow the chemical space for the analogue(s) search.

The tool was developed within the project [EU-ToxRisk](https://www.eu-toxrisk.eu/) and it is based on the KNIME workflow available [here](https://github.com/DGadaleta88/RAX_tool) by Domenico Gadaleta.

## Installation

### Installing and starting RAXpy

The software is a free and open-source application (GUI). It works on Windows and Linux operating system. Download and unpack the zipped file according your operating system:

*	RAXpy Win v1.0.0 (64bit).zip (Windows)

*	RAXpy Linux v1.0.0 (64bit).tar.gz (Linux)

To start the application, move to the application folder and run the executable file RAXpy.

### How to generate an executable file

#### Installing dependencies

The software was developed in Python 3 and uses the following libraries: 

tkinter 8.6.10 

rdkit 2020.03.6 

pandas 1.2.0

sygma 1.1.0 

scipy 1.6.0 

pubchempy 1.0.4

#### Generating an executable file

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install PyInstaller.

```bash
pip install pyinstaller
```

Use the PyInstaller package [pyinstaller](https://pypi.org/project/pyinstaller/) to create the executable. Go to the directory where the Python script is stored.

For Windows system run:

```bash
pyinstaller main.py --name RAXpy --add-data "datasets\DILIRank_input.csv;datasets" --add-data "datasets\ToxRef_input.csv;datasets" --add-data "sygma\rules\*.txt;sygma\rules"
```

In Linux run:

```bash
pyinstaller main.py --name RAXpy --add-data "datasets/DILIRank_input.csv:datasets" --add-data "datasets/ToxRef_input.csv:datasets" --add-data "sygma/rules/*.txt:sygma/rules"
```

Then, add in the *.spec file the following:

import sys

sys.setrecursionlimit(2000) 

Now run the spec file using:

```bash
pyinstaller filename.spec
```

Your executable should now get created at the location. To find the executable file, open the dist folder.

## Usage

You can read the [RAXpy User Guide](https://bitbucket.org/giolavado/raxpy/src/master/RAXpy%20User%20Guide.pdf)

## Reference

Gadaleta, D., Golbamaki, B. A., Lavado, G. J., Roncaglioni, A., & Benfenati, E. (2020). Automated Integration of Structural, Biological and Metabolic Similarities to Sustain Read-Across. ALTEX. 9 May 2020. https://doi.org/10.14573/altex.2002281

## Contributing and credits

RAXpy tool has been developed within the European Union's Horizon 2020 research and innovation programme under Grant Agreement No. 681002 ([EU-ToxRisk](https://www.eu-toxrisk.eu/)).

RAXpy software was originally created by Giovanna Janet Lavado at Laboratory of Environmental Chemistry and Toxicology, Department of Environmental Health Sciences, Istituto di Ricerche Farmacologiche Mario Negri - IRCCS. 

## License
RAXpy is licensed under [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)