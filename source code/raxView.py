# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2021- Giovanna Janet Lavado
#
# This file is part of the RAXpy.
# The contents are covered by the terms of the GNU General Public License
# which is included in the file LICENSE.txt, found at the root
# of the RAXpy source tree.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    
# -----------------------------------------------------------------------------

"""
@author: G.J. Lavado
"""

import tkinter as tk # python 3
from pathlib import Path
from tkinter.filedialog import askdirectory

import logging

class View:
    def __init__(self, root, ctrl):
        """ Initialize variables."""
        
        self.logger = logging.getLogger('rax.View')
        self.logger.info('creating an instance of View')
        
        self.root = root
        self.ctrl = ctrl
        
        # default values
        self.entered_id_target = tk.StringVar()
        self.entered_id_target.set("Amlodiine")
        
        self.entered_smi_target = tk.StringVar()
        # CC(C)(C)NCC(O)COc1cccc2c1CCCC2=O # ToxRef 
        # CCOC(=O)C1=C(NC(=C(C1C2=CC=CC=C2Cl)C(=O)OC)C)COCCN # DILI
        self.entered_smi_target.set("CCOC(=O)C1=C(NC(=C(C1C2=CC=CC=C2Cl)C(=O)OC)C)COCCN")
       
        self.entered_n_str = tk.IntVar()
        self.entered_n_str.set(10) 
        self.entered_n_bio = tk.IntVar()
        self.entered_n_bio.set(5)
        self.entered_n_meta = tk.IntVar()
        self.entered_n_meta.set(5)
        
        self.var_filter_mcs = tk.IntVar()
        self.var_filter_mcs.set(1)
        self.entered_thd_mcs = tk.DoubleVar() 
        self.entered_thd_mcs.set(0.5)
        
        self.var_filter_fg = tk.IntVar()
        self.var_filter_fg.set(1)
        self.entered_thd_fg = tk.DoubleVar() 
        self.entered_thd_fg.set(0.6)
        
        self.var_dataset = tk.IntVar()
        self.var_dataset.set(1)
        
        # default path output
        self._results_folder=  Path.cwd() # / "RAX_results" # default
 
        self._setup()

    def _setup(self):
        """ Calls methods to setup the user interface."""
        self._create_menu()
        self._create_widgets()
        self._setup_layout()
        
    def _create_menu(self):
        self.menu_bar = tk.Menu(self.root)
        
        self.help_menu = tk.Menu(self.menu_bar, tearoff=0)
        #self.help_menu.add_command(label="Manual")
        self.help_menu.add_command(label="About RAXpy...", command=self.about_win)
        
        self.menu_bar.add_cascade(label="Help", menu=self.help_menu)
        self.root.config(menu=self.menu_bar)
        
    def _create_widgets(self):
        # Create a new frame `frm_input` to contain the Label
        # and Entry widgets for entering SMILES information.
        self.frm_input = tk.LabelFrame(self.root, text="Insert SMILES")
        
        # Create the Label and Entry widgets for "ID" and "SMILES"
        # ID
        self.lbl_ID = tk.Label(self.frm_input, text ="ID:")
        self.ent_ID = tk.Entry(self.frm_input, textvariable=self.entered_id_target, width=50)
        
        #SMILES
        self.lbl_SMILES = tk.Label(self.frm_input, text ="SMILES:")
        self.ent_SMILES = tk.Entry(master=self.frm_input, textvariable= self.entered_smi_target, width=50)
        
        # Create a new frame `frm_params` to contain the Label
        # and Entry widgets for entering parameters.
        self.frm_params = tk.LabelFrame(self.root, text="Set parameters")
        
        # Neighbors
        self.title_negbrs = tk.Label(self.frm_params, text ="Number of neighbors in the similarity lists")

        
        self.lbl_negbrs_str = tk.Label(self.frm_params, text ="N. of neighbors (Structural similarity):")

        vcmdStrSim = self.frm_params.register(self.validateStrSim) # we have to wrap the command
        self.sb_n_str = tk.Spinbox(self.frm_params, textvariable=self.entered_n_str, 
                                    from_=1, to=20, justify=tk.RIGHT, 
                                    validate="key", validatecommand=(vcmdStrSim, '%P')) 

        self.lbl_negbrs_bio = tk.Label(self.frm_params, text ="N. of neighbors (Biological similarity):")
        vcmdBioSim = self.frm_params.register(self.validateBioSim) # we have to wrap the command
        self.sb_n_bio = tk.Spinbox(self.frm_params, textvariable=self.entered_n_bio, 
                                   from_=1, to=20, justify=tk.RIGHT,
                                   validate="key", validatecommand=(vcmdBioSim, '%P')) 
        
        self.lbl_negbrs_meta = tk.Label(self.frm_params, text ="N. of neighbors (Metabolic similarity):")

        vcmdMetaSim = self.frm_params.register(self.validateMetaSim) # we have to wrap the command
        self.sb_n_meta = tk.Spinbox(self.frm_params, textvariable=self.entered_n_meta, from_=1, to=20, justify=tk.RIGHT,
                                    validate="key", validatecommand=(vcmdMetaSim, '%P')) 

        # Maximum common substructures MCS
        self.title_mcs = tk.Label(self.frm_params, text ="Maximum common substructures (MCS) filter")
        self.lbl_mcs = tk.Label(self.frm_params, text ="Do you consider MCS?")
        self.chkbtn_mcs = tk.Checkbutton(self.frm_params, text = "", 
                                         variable=self.var_filter_mcs,
                                          command=self.select_mcs)
        
        self.lbl_ratio_mcs = tk.Label(self.frm_params, text = "MCS (% size target):")
        vcmdThdMcs = self.frm_params.register(self.validateThdMcs) # we have to wrap the command
        self.sb_n_mcs = tk.Spinbox(self.frm_params, textvariable= self.entered_thd_mcs, 
                                   from_=0.0, to=1.0, increment=0.1, justify=tk.RIGHT,
                                   validate="key", validatecommand=(vcmdThdMcs, '%P')) 
       
        # Functional groups FGs
        self.title_fg = tk.Label(self.frm_params, text ="Common functional groups (FG) filter")

        self.lbl_fg = tk.Label(self.frm_params, text ="Do you consider FG?")
        self.chkbtn_fg = tk.Checkbutton(self.frm_params, text = "", variable=self.var_filter_fg,
                                          command=self.select_fg)
        
        self.lbl_ratio_fg = tk.Label(self.frm_params, text = "FG (% size target):")
        vcmdThdFGs = self.frm_params.register(self.validateThdFGs) # we have to wrap the command
        self.sb_n_fg = tk.Spinbox(self.frm_params, textvariable= self.entered_thd_fg, 
                                  from_=0.0, to=1.0, increment=0.1, justify=tk.RIGHT,
                                  validate="key", validatecommand=(vcmdThdFGs, '%P')) 
        
        # Create a new frame `frm_dataset_source` to contain the Label and Radiobutton widgets for selecting dataset.
        self.frm_dataset_source = tk.LabelFrame(self.root, text="Select a dataset")
        
        self.rbtn_dataset1 = tk.Radiobutton(self.frm_dataset_source, text="Human hepatotoxicity (ToxRef)", 
                                            variable=self.var_dataset, value=0 , command=self.select_dataset)
        self.rbtn_dataset2 = tk.Radiobutton(self.frm_dataset_source, text="Drug-induced liver injury (DILI)", 
                                            variable=self.var_dataset, value=1 , command=self.select_dataset)
        
        # frame buttons
        self.frm_buttons = tk.Frame(self.root)
        self.btn_load = tk.Button(self.frm_buttons, text="Load dataset", command=self.load_dataset)
        self.btn_run = tk.Button(self.frm_buttons, text="Run RAX", command=self.run)
        self.btn_run['state'] = tk.DISABLED
        self.btn_save = tk.Button(self.frm_buttons, text="Save results", command=self.save)
        self.btn_save['state'] = tk.DISABLED
                
        # Create a new frame `frm_txt_box` to contain the Text widget for containing log
        self.frm_txt_box = tk.Frame(self.root)
        self.txt_box = tk.Text(self.frm_txt_box)
        self.scrollbar = tk.Scrollbar(self.frm_txt_box)
        self.txt_box.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.txt_box.yview)
  
    def _setup_layout(self):
        
        # frm_input
        self.frm_input.pack(fill=tk.BOTH, expand=1, padx=5, pady=7)
        
        # Use the grid geometry manager to place the Label and
        # Entry widgets in the first and second columns of the
        # first row of the grid
        self.lbl_ID.grid(row=0, column=0, sticky="e", padx=5, pady=5)
        self.ent_ID.grid(row=0, column=1)
        self.lbl_SMILES.grid(row=1, column=0, sticky="e", padx=5, pady=5)
        self.ent_SMILES.grid(row=1, column=1)
        
        # frm_params
        self.frm_params.pack(fill=tk.BOTH, expand=1, padx=5, pady=7)
        
        self.title_negbrs.grid(row=0, column=0, sticky="e", padx=5, pady=7)

        self.lbl_negbrs_str.grid(row=1, column=0, sticky="e", padx=5, pady=5)
        self.sb_n_str.grid(row=1, column=1)
        
        self.lbl_negbrs_bio.grid(row=2, column=0, sticky="e", padx=5, pady=5)
        self.sb_n_bio.grid(row=2, column=1)
        
        self.lbl_negbrs_meta.grid(row=3, column=0, sticky="e", padx=5, pady=5)
        self.sb_n_meta.grid(row=3, column=1)
        
        # MCS
        self.title_mcs.grid(row=4, column=0, sticky="e", padx=5, pady=7)
        
        self.lbl_mcs.grid(row=5, column=0, sticky="e", padx=5, pady=5)
        self.chkbtn_mcs.grid(row=5, column=1, sticky="w")
        
        self.lbl_ratio_mcs.grid(row=6, column=0, sticky="e")
        self.sb_n_mcs.grid(row=6, column=1)
    
        # FG
        self.title_fg.grid(row=7, column=0, sticky="e", padx=5, pady=7)

        self.lbl_fg.grid(row=8, column=0, sticky="e", padx=5, pady=5)
        self.chkbtn_fg.grid(row=8, column=1, sticky="w")
        
        self.lbl_ratio_fg.grid(row=9, column=0, sticky="e", padx=5, pady=5)
        self.sb_n_fg.grid(row=9, column=1)
        
        # frm_dataset_source
        self.frm_dataset_source.pack(fill=tk.BOTH, expand=1, padx=5, pady=7)
        self.rbtn_dataset1.grid(row=0, column=0, sticky="w", padx=5, pady=5)
        self.rbtn_dataset2.grid(row=1, column=0, sticky="w", padx=5, pady=5)
        
        # frm_buttons
        self.frm_buttons.pack(fill=tk.BOTH, expand=1, padx=5, pady=7)
        self.btn_load.grid(row=0, column=0, sticky="w", padx=5, pady=3)
        self.btn_run.grid(row=0, column=1, sticky="w", padx=5, pady=3)
        self.btn_save.grid(row=0, column=2, sticky="w", padx=5, pady=3)
        
        # frm_textbox
        self.frm_txt_box.pack(fill=tk.BOTH, expand=1, padx=5, pady=5)
        self.txt_box.pack(side='left', fill=tk.BOTH, expand=1, padx=5, pady=5)
        self.scrollbar.pack(side='right', fill='y')

    def load_dataset(self):
        self.logger.info('view - load dataset')
        self.ctrl.loadDatasetButtonPressed()
        #self.summary()
    
    def run(self):
        self.logger.info('view - run')
        self.update_txt_box(self.summary())
        
        self.ctrl.runButtonPressed()
        
        #self.txt_box.yview(tk.END)

    def save(self):
        self.logger.info('view - save')
        path_selected = askdirectory(initialdir = self._results_folder)
        self.ctrl.saveButtonPressed(path_selected)
            
    def update_txt_box(self, txt):
        self.txt_box.insert("end", txt)
        self.txt_box.yview(tk.END)
        
    def select_mcs(self):
        
        if self.var_filter_mcs.get() == 0:
            self.sb_n_mcs['state'] = tk.DISABLED
        else:
            self.sb_n_mcs['state'] = tk.NORMAL
        
    def select_fg(self):
        
        if self.var_filter_fg.get() == 0:
            self.sb_n_fg['state'] = tk.DISABLED
        else:
            self.sb_n_fg['state'] = tk.NORMAL
        
    def select_dataset(self):
        
        self.btn_run['state'] = tk.DISABLED
        self.btn_save['state'] = tk.DISABLED
  
    def validateStrSim(self, new_text):
        if not new_text: # the field is being cleared
            self.entered_n_str = 10
#            self.sb_n_str.delete(0,"end")
#            self.sb_n_str.insert(0,self.entered_n_str)
            return True
    
        try:
            val = int(new_text)
            if val >= 1: 
                self.entered_n_str = val
                return True
            else:
                return False
        except ValueError:
            return False

    def validateBioSim(self, new_text):
        if not new_text: # the field is being cleared
            self.entered_n_bio = 5
            return True
    
        try:
            val = int(new_text)
            if val >= 1:
                self.entered_n_bio = val
                return True
            else:
                return False
        except ValueError:
            return False

    def validateMetaSim(self, new_text):
        if not new_text: # the field is being cleared
            self.entered_n_meta = 5
            return True
    
        try:
            val = int(new_text)
            if val >= 1:
                self.entered_n_meta = val 
                return True
            else:
                return False
        except ValueError:
            return False

    def validateThdMcs(self, new_text): 
        if not new_text: # the field is being cleared
            self.entered_thd_mcs = 0.5
            return True
    
        try:
            val = float(new_text) 
            if 0 <= val <= 1:
                self.entered_thd_mcs = val 
                return True
            else:
                 return False
        except ValueError:
            return False

    def validateThdFGs(self, new_text): 
        if not new_text: # the field is being cleared
            self.entered_thd_fg = 0.6
            return True
    
        try:
            val = float(new_text) 
            if 0 <= val <= 1:
                self.entered_thd_fg = val 
                return True
            else:
                 return False
        except ValueError:
            return False    
        
    def summary(self):
        # SMILES info
        selection =  '\nID: ' + self.entered_id_target.get() + '\n'
        selection += 'Input: ' + self.entered_smi_target.get() + '\n'

        # Parameters
        selection += 'N. of neighbors in the structural similarity list: ' + str(self.entered_n_str) + '\n'
        selection += 'N. of neighbors in the biological similarity list: ' +  str(self.entered_n_bio) + '\n' #self.sb_n_bio.get() + '\n'
        selection += 'N. of neighbors in the metabolic similarity list: ' +  str(self.entered_n_meta) + '\n' #self.sb_n_meta.get() + '\n'
        
        if self.var_filter_mcs.get() == 1:
            selection += 'MCS (% size target): ' + str(self.entered_thd_mcs) + '\n'

        if self.var_filter_fg.get() == 1:
            selection += 'FG (% size target): ' + str(self.entered_thd_fg) + '\n'
        
        # selected dataset
        selection += "Selected dataset: "
        if self.var_dataset.get()==0:
                selection += 'ToxRef' + '\n'
        else:
            selection += 'DILI' + '\n'
        
        selection+= '\n'
        
        #self.txt_box.delete("1.0","end")
        #self.txt_box.insert("end", selection)
        
        return selection
        
    def about_win(self):
       about_win = tk.Toplevel(self.root)
       about_win.title("About RAXpy")
       about_win.minsize(600,150)
              
       content = tk.Frame(about_win) 
       frame = tk.Frame(content, borderwidth=1, relief="groove", width=90, height=150)
       
       raxpy = 'RAXpy'
       versione = 'v. 1.0.0'
       desc = 'A read-across tool based on structural, biological and metabolic similarities.'
       
       
       _license = 'The application is released under the term of the GNU-GPL-3 license.'
       created = 'Giovanna Janet Lavado'
       developed = 'Istituto di Ricerche Farmacologiche Mario Negri - IRCCS.'
       soft_used = 'The software was developed in Python 3 and uses the following libraries: tkinter v. 8.6.10, rdkit v. 2020.03.6, pandas v. 1.2.0, sygma v. 1.1.0, scipy v. 1.6.0, pubchempy v. 1.0.4'
      
       label_raxpy = tk.Label(content, text=raxpy + ' '+ versione, font=('bold'))
       label_desc = tk.Label(content, text=desc)

       label_license = tk.Label(content, text='License:')
       the_license = tk.Label(content, text=_license, justify = tk.LEFT)
       
       label_created = tk.Label(content, text='Created by:')
       created = tk.Label(content, text=created + ', ' + developed + '\nThe tool is based on the KNIME workflow available at https://github.com/DGadaleta88/RAX_tool by Domenico Gadaleta.', wraplength=450, justify = tk.LEFT)
       
       label_soft_used = tk.Label(content, text=soft_used, wraplength=600,  justify = tk.CENTER)
      
      
       content.grid(column=0, row=0)
#       frame.grid(column=0, row=0, columnspan=1, rowspan=5)
 
       label_raxpy.grid(row=0, column=1, columnspan=2)
       label_desc.grid(row=1, column=1,  columnspan=2)
       
       label_license.grid(row=2, column=1, sticky="w", padx=15)
       the_license.grid(row=2, column=2, sticky="w")
       
       label_created.grid(row=3, column=1, sticky="w", padx=15)
       created.grid(row=3, column=2, sticky="w")
       
       label_soft_used.grid(row=4, column=1, columnspan=2, sticky="w", pady=15)
 