# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2021- Giovanna Janet Lavado
#
# This file is part of the RAXpy.
# The contents are covered by the terms of the GNU General Public License
# which is included in the file LICENSE.txt, found at the root
# of the RAXpy source tree.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    
# -----------------------------------------------------------------------------

"""
@author: G.J. Lavado
"""
from pathlib import Path
import pandas as pd
from pandas.errors import ParserError
from rdkit import Chem
from Util import MCSFilter, FGFilter, structuralFilter, biologicalFilter, metabolicFilter, merge_lists, fullAnalogues, predictions
import xlsxwriter

import logging

class Model:

    def __init__(self, ctrl):
        """ Initialize variables."""
        
        self.logger = logging.getLogger('rax.Model')
        self.logger.info('creating an instance of Model')
        
        self.ctrl = ctrl

        self.is_loaded_dataset = 0 # if a source_dataset is loaded
        self._df_source_dataset = pd.DataFrame() # current df_source_dataset
        
        # path input
        # datasets have four columns and a header: id, name, SMILES and activity 
        self._dataset_folder = Path.cwd() / "datasets"
        #self.path_in = '' # current source_dataset path
        
        # default path output
        #self.results_folder=  Path.cwd() / "RAX_results" # default
        
        self.myLOG = ''
 
        # print(self._dataset_folder)
        self.is_calculated = 0 # if rax calculation is done
        self._df_out_filtered_dataset = pd.DataFrame() # current filtered dataset
        self._df_out_str = pd.DataFrame() # current structural similarity list
        self._df_out_bio = pd.DataFrame() # current bioloogical similarity list
        self._df_out_meta = pd.DataFrame() # current metabolic similarity list
        self._df_out_all_analogues = pd.DataFrame() # all analogues
        self._df_out_predictions = pd.DataFrame() # predictions
        
    #Delegates-- Model would call this on internal change
    def dataset_changed(self):
        self.logger.info('model - dataset_changed')
        
        self.ctrl.datasetChangedDelegate()
        
    def calculation_rax_changed(self):
        self.logger.info('model - calculation_rax_changed')
       
        self.ctrl.calculationRaxChangedDelegate()
    
    def save_results_changed(self):
        self.logger.info('model - save_results_changed')
       
        self.ctrl.saveResultsChangedDelegate()
        
    def load_dataset(self, val):
        self.logger.info('model - load_dataset')
 
        self.is_loaded_dataset = 0     
        
        # dataset has to contain four columns in this order: id, SMILES, activity e name
        if val == 0: path_in = self._dataset_folder / 'ToxRef_input.csv'
        elif val == 1: path_in = self._dataset_folder / 'DILIRank_input.csv'
        # elif val == 2: 
        
        #print(path_in)
        
        self._df_source_dataset = self._load_dataset(path_in)
        
        if self._df_source_dataset is not None:
            self.is_loaded_dataset = 1
            self.logger.debug('Source dataset - columns: %s' % self._df_source_dataset.columns) 
            #print(self._df_source_dataset.head(8))
        
        self.dataset_changed()        
    
    def _load_dataset(self, path):
        #self.logger.info('model - _load_dataset')
                
        retval = 0
        df = None
 
       # create three boolean variables for each kind of exceptions.
        parse_error = False
        file_not_found = False
        file_empty = False
        
        try:
            # dataset have to contain four columns and a header: id, SMILES, activity and name
            df = pd.read_csv(path)
            
        # if the error is ParserError, print file has incorrect format and set parse_error = True
        except ParserError:
            parse_error = True
            df = None
            
        # if the error is FileNotFoundError, print file is missing and set file_not_found = True
        except FileNotFoundError:
            file_not_found = True
            df = None
 
        # if no exception occurs and not empty, set retval = 1.
        else:
            n_rows = len(df)
            self.logger.debug('n. rows: %d' % n_rows)
            
            if not n_rows: # if empty
                file_empty = True
                df = None
            else:
                retval = 1
                
                # check columns
                
                # define an internal ID
                system_id_index = []
                for i in range(n_rows):
                   system_id_index.append('id' + str(i))

                df.insert(0,'raxID',system_id_index)
                
                # print(df.head(8))
            
        # Now, in the end, update the log.
        finally:
            msg = str(path) + ' '  
            if parse_error: msg += 'has incorrect format'
            elif file_not_found: msg += 'is missing'
            elif file_empty: msg+= 'is empty'
            elif retval: msg += 'has been loaded'
            
        self.logger.debug(msg)
                
        self.myLOG = msg + '.\n'
        
        return df
    
    def calculate(self, id_target, smi_target, n_str, n_bio, n_meta, filter_mcs, thd_mcs, filter_fg, thd_fg):
        self.logger.info('model - calculate')
        
        # pd.set_option('mode.chained_assignment','raise')
         
        self.is_calculated = 0 # final results are ready to save?
        
        self.logger.debug(id_target + ' ' + smi_target)
        self.logger.debug('No. neighbors in str list: {}, bio list: {}, meta list: {}'.format(n_str,n_bio,n_meta))
        self.logger.debug('MCS filter? {}, threshold: {}'.format(filter_mcs,thd_mcs))
        self.logger.debug('FG filter? {}, threshold: {}'.format(filter_fg,thd_fg))
 
        # read RDKit molecule
        mol_target = Chem.MolFromSmiles(smi_target)
 
        molfromsmiles_error = False
        if mol_target is None or len(smi_target)==0:  
            molfromsmiles_error = True
        else:
            # unique Excel file with 6 sheets: filtered dataset, structural, metabolical, biological analogues and full list of analogue(s). 
            # Moreover, predictions
            
            # select raxID and SMILES columns
            df_for_filter = self._df_source_dataset.iloc[:, [0, 2]]
  #          print(df_for_filter)
            
            # df_filtered_dataset contains final filtered dataset to use for similarity calculations
            df_filtered_dataset = df_for_filter # initial situation
        
            # compute MCS filter
            # RDKit MCS
            if filter_mcs:
                df_MCS_result = MCSFilter(mol_target, df_for_filter, thd_mcs) # could be empty
 
            # compute FG filter
            if filter_fg:
                df_FG_result = FGFilter(mol_target, df_for_filter, thd_fg) # could be empty
  
            # calculate filtered_dataset
            if filter_mcs and filter_fg: 
                # merge based on the common columns (raxID and SMILES)
                merge_result = pd.merge(df_FG_result, df_MCS_result, on=None, left_index=False, right_index=False,
                                        indicator=False, validate='one_to_one')    
                df_filtered_dataset = merge_result[(merge_result['CFGs?']== 1) & (merge_result['MCS?']== 1)]
            elif filter_mcs:  
                df_filtered_dataset = df_MCS_result[df_MCS_result['MCS?'] == 1]
            elif filter_fg:                 
                df_filtered_dataset = df_FG_result[df_FG_result['CFGs?'] == 1]
  
            # df_filtered_dataset contains at least raxID and SMILES columns and could be empty
            # if df_filtered_dataset is empty there are no molecules to process in the next phases
            #self.logger.debug('Filtered dataset - no. rows: %d' % len(df_filtered_dataset))
            #self.logger.debug('Filtered dataset - columns: %s' %  df_filtered_dataset.columns)
            
            # OUTPUT 0
            # merge based on the common columns (raxID and SMILES), default how = inner
            self._df_out_filtered_dataset = pd.merge(self._df_source_dataset, df_filtered_dataset, on=None, left_index=False, right_index=False,
                                        indicator=False, validate='one_to_one')
            self.logger.debug('Merge filtered  dataset - no. rows: %d' % len(self._df_out_filtered_dataset))
            self.logger.debug('Merge filtered dataset - columns: %s' %  self._df_out_filtered_dataset.columns)
            
            # select raxID and SMILES columns from df_filtered_dataset
            df_for_filter =  df_filtered_dataset.iloc[:, [0, 1]] 
            
            #print(df_for_filter.head)
            
            # OUTPUT 1 
            # compute structural similarity list
            # RDKit MACCS fingerprint, Tanimoto distance
            # input: mol_target, df_for_filter
            # df_for_filter have to contain as first and second the raxID and SMILES columns in this order
            # temporary info about structural aspect
            df_str = structuralFilter(mol_target, df_for_filter) # select similarity  > 0 and data are ranked
            # select rows with Rank <= n_str
            self._df_out_str = pd.DataFrame(df_str[df_str['Rank'] <= n_str])        
            # flag Rank = 'STR' + Rank        
            self._df_out_str['Rank'] =self._df_out_str.apply(lambda x : 'STR' + str(x['Rank']),axis=1)
            
            # OUTPUT 2
            # compute bioloogical similarity list
            # PubChem assays, query request 
            # dataframe have to contain raxID and SMILES columns in this order
            # temporary info about biological aspect
            df_bio = biologicalFilter(smi_target, df_for_filter) # select similarity  > 0 and data are ranked
            # select rows with Rank <= n_bio
            self._df_out_bio =  pd.DataFrame(df_bio[df_bio['Rank'] <= n_bio])
            # flag Rank = 'BIO' + Rank
            self._df_out_bio['Rank'] =  self._df_out_bio.apply(lambda x : 'BIO' + str(x['Rank']),axis=1)
           
            # OUTPUT 3
            # compute metabolic similarity list
            # SyGMa predictions
            # dataframe have to contain raxID and SMILES columns in this order
            # temporary info about metabolic aspect
            df_meta = metabolicFilter(mol_target, df_for_filter) # select similarity  > 0 and data are ranked
            # select rows with Rank <= n_meta
            self._df_out_meta =  pd.DataFrame(df_meta[df_meta['Rank'] <=  n_meta])
            # flag Rank = 'BIO' + Rank
            self._df_out_meta['Rank'] =  self._df_out_meta.apply(lambda x : 'MET' + str(x['Rank']),axis=1)
            
            # OUTPUT 4
            # complete list of analogues
            # dataframe all_similarity_data contains all similarity data for each compound in the filtered dataset
            all_similarity_data = merge_lists(self._df_out_filtered_dataset, df_str, df_bio, df_meta) # if empty?
            
            # Full list of analogues is based on the selected no. neighbors in the similarity lists 
            self._df_out_all_analogues = fullAnalogues(id_target, mol_target, self._df_out_str, self._df_out_bio, self._df_out_meta, all_similarity_data)
            
            # OUTPUT 5
            # compute predictions
            self._df_out_predictions = predictions(id_target, mol_target, self._df_out_all_analogues)
    
            self.is_calculated = 1
           
        msg = 'Target SMILES "' + smi_target + '" '
        if molfromsmiles_error: msg += 'has incorrect format'
        elif self.is_calculated: msg += 'has been processed'
        
        self.logger.debug(msg)
                
        self.myLOG = msg + '.\n'
        
        self.calculation_rax_changed()
        
    def save_results(self, path):
        self.logger.info('model - save_results')
               
        # save an Excel file
           
        if len(path) > 0 and self.is_calculated: 
            path_output = Path(path) / "RAX_results.xlsx"
             
            try:
                with pd.ExcelWriter(path_output) as writer:  
                    self._df_out_filtered_dataset.to_excel(writer, sheet_name='Filtered dataset', index=False)
                    self._df_out_str.to_excel(writer, sheet_name='Structural analogue(s)', index=False)
                    self._df_out_bio.to_excel(writer, sheet_name='Biological analogue(s)', index=False)
                    self._df_out_meta.to_excel(writer, sheet_name='Metabolic analogue(s)', index=False)
                    self._df_out_all_analogues.to_excel(writer, sheet_name='Full list analogue(s)', index=False) 
                    self._df_out_predictions.to_excel(writer, sheet_name='Prediction(s)', index=True, index_label='groups_threshold')  
                    
            # catch both exceptions xlsxwriter.exceptions.FileCreateError for engine xlsxwriter  
            # and PermissionError for engine openpyxl
            except (xlsxwriter.exceptions.FileCreateError, PermissionError):
                msg = 'Please, if the file is open in Excel, close it.\nTry to save the file again'
                self.logger.debug('Exception caught - close the file')
            # if no exception occurs
            else:                
                msg = 'Results have been saved in the directory ' + path

            self.logger.debug(msg)
                
            self.myLOG = msg + '.\n'
            
            self.save_results_changed()
            
