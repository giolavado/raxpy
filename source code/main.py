# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2021- Giovanna Janet Lavado
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# (see LICENSE.txt for details)
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    
# -----------------------------------------------------------------------------

"""
RAXpy, a tool for read-across based on structural, biological and metabolic similarities.
=====================================================
Copyright (C) 2021- Giovanna Janet Lavado

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
(see LICENSE.txt for details)

@author: G.J. Lavado

"""

from raxController import Controller

if __name__ == '__main__':
          
    c = Controller()
    c.run()
   
    
 
    
    