# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2021- Giovanna Janet Lavado
#
# This file is part of the RAXpy.
# The contents are covered by the terms of the GNU General Public License
# which is included in the file LICENSE.txt, found at the root
# of the RAXpy source tree.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    
# -----------------------------------------------------------------------------

"""Utility functions for the RAXpy application."""

from rdkit import Chem, DataStructs
from rdkit.Chem import rdFMCS, MACCSkeys
import sygma
import pandas as pd
from scipy.stats.mstats import mode

from io import BytesIO
from urllib.error import HTTPError, URLError
import pubchempy as pcp
from pubchempy import PubChemHTTPError

import logging

# create logger
# # Each instance has a name, and they are conceptually arranged in a namespace hierarchy using dots
# (periods) as separators. For example, a logger named ‘scan’ is the parent of loggers ‘scan.text’, ‘scan.html’ and ‘scan.pdf’. 
# Logger names can be anything you want, and indicate the area of an application in which a logged message originates.
module_logger = logging.getLogger('rax.raxModel.util')

    
def MCS(target, mol):
    """Find a maximum common substructure (MCS) of two molecules.
    
    Parameters
    ----------
    target : RDKit molecule
        First molecule to be found a MCS.
    mol : RDKit molecule
        Second molecule to be found a MCS.

    Returns
    -------
    res : MCSResult instance
        It returns an MCSResult instance with information about the number of atoms and bonds 
        in the MCS, the SMARTS string which matches the identified MCS, and a flag saying if the algorithm timed out. 
        If no MCS is found then the number of atoms and bonds is set to 0 and the SMARTS to ''.
    """
    
    
    mols = [target,mol]# vedere se conviene qui oppure nel chiamante
    # MCS between target and mol
    # FindMCS function find a maximum common substructure (MCS) of two or more molecules:
    res=rdFMCS.FindMCS(mols, atomCompare=rdFMCS.AtomCompare.CompareElements, 
                        bondCompare=rdFMCS.BondCompare.CompareOrderExact, 
                        matchValences=True,
                        ringMatchesRingOnly=True,
                        completeRingsOnly=True,
                        timeout=300)
     #The MCS algorithm will exhaustively search for a maximum common substructure. 
     #Typically this takes a fraction of a second, but for some comparisons this can take minutes or longer. 
     #Use the timeout parameter to stop the search after the given number of seconds (wall-clock seconds, not CPU seconds) and return the best match found in that time. If timeout is reached then the canceled property of the MCSResult will be True instead of False.

    return res
    
    # target molecule, dataframe and threshold
    # dataframe: ID and SMILES columns
def MCSFilter(mol_target, df, thsd):
    """
    Filter a Pandas DataFrame of SMILES based on the presence of a MCS with respect to the target molecule using a percentage specified as threshold. 
    
    If the ratio of the number of atoms in the MCS and number of heavy atoms for both, target and the analogue, is greater than the given threshold,
    then the analogue is flagged with 1 for the following searches.  
    
    Parameters
    ----------
    mol_target : RDKit molecule
        A target molecule.
    df : Pandas DataFrame
        A dataframe containing identifiers (ID) and SMILES in the first and second column, respectively.
    thsd : double
        A threshold between 0 and 1.

    Returns
    -------
    df_MCS : Pandas DataFrame
        A dataframe containing information about the SMARTS string, the number of atoms and bonds in the MCS, calculated ratios 
        and relative status for each analogue in the given dataframe.  

    """
    module_logger.info('received a call to "MCSFilter"')

    # select columns
    df_MCS = df.iloc[:, [0, 1]] # select raxID and SMILES
    mcs_smart = []
    mcs_num_atoms = []
    mcs_num_bonds = []
    #num_heavy_atoms_target = []
    #num_heavy_atoms_source = []
    ratio_mcs_target = []
    ratio_mcs_source = []
    status = []
    
    for i in df_MCS.index:
        smi_other = df_MCS.loc[i].iat[1] # SMILES

        mol_other = Chem.MolFromSmiles(smi_other)  
        mcs = MCS(mol_target, mol_other)
        mcs_smart.append(mcs.smartsString)
        mcs_num_atoms.append(mcs.numAtoms)
        mcs_num_bonds.append(mcs.numBonds)

        # ratio between n. atoms of MCS and n. heavy atoms count of target
        ratioMCSTarget = mcs.numAtoms / mol_target.GetNumHeavyAtoms()
        # ratio between n. atoms of MCS and n. heavy atoms count of source
        ratioMCSSource = mcs.numAtoms / mol_other.GetNumHeavyAtoms()
        ratio_mcs_target.append(ratioMCSTarget)
        ratio_mcs_source.append(ratioMCSSource)
        
        #num_heavy_atoms_target.append(mol_target.GetNumHeavyAtoms())
        #num_heavy_atoms_source.append(mol_other.GetNumHeavyAtoms())

        # status - there exist MCS?    
        is_included = 0  
        if ratioMCSTarget >= thsd and ratioMCSSource >= thsd:
            is_included = 1 # incluso nel dataset filtrato
    
        status.append(is_included)

    # add lists as column in the final df
    df_MCS['MCS smart'] = mcs_smart
    df_MCS['MCS no. atoms'] = mcs_num_atoms
    df_MCS['MCS no. bonds'] = mcs_num_bonds
    #df_MCS['No. heavy atoms target'] = num_heavy_atoms_target
    #df_MCS['No. heavy atoms source'] = num_heavy_atoms_source
    df_MCS['ratio MCS target'] = ratio_mcs_target
    df_MCS['ratio MCS source'] = ratio_mcs_source
    df_MCS['MCS?'] = status
    
    return df_MCS

def calculate_FG(mol):
    """Find functional groups of the given RDKit molecules.
    Presence of 22 functional groups codified as SMARTS is verified."""
    
    RDKit_SMARTS_df = pd.DataFrame(
    {'SMARTS': ['[$(C-!@[a])](=O)(Cl)',
                '[$(C-!@[A;!Cl])](=O)(Cl)',
                '[$(C-!@[a])](=O)([O;H,-])',
                '[$(C-!@[A;!O])](=O)([O;H,-])',
                '[$(C-[C;!$(C=[!#6])]-[N;!H0;!$(N-[!#6;!#1]);!$(N-C=[O,N,S])])](=O)([O;H,-])',
                '[$(S-!@c)](=O)(=O)(Cl)',
                '[$(S-!@C)](=O)(=O)(Cl)',
                '[N;!H0;$(N-c);!$(N-[!#6;!#1]);!$(N-C=[O,N,S])]',
                '[N;!H0;!$(N-c);$(N-C);!$(N-[!#6;!#1]);!$(N-C=[O,N,S])]',
                '[N;!H0;R;$(N-[#6]);!$(N-[!#6;!#1]);!$(N-C=[O,N,S])]',
                '[$(B-!@c)](O)(O)',
                '[$(B-!@C)](O)(O)',
                '[$(N-!@c)](=!@C=!@O)',
                '[$(N-!@C)](=!@C=!@O)',
                '[O;H1;$(O-!@c)]',
                '[O;H1;$(O-!@[C;!$(C=!@[O,N,S])])]',
                '[CH;D2;$(C-!@[a])](=O)',
                '[CH;D2;$(C-!@C)](=O)',
                '[F,Cl,Br,I;$(*-!@c)]',
                '[$([F,Cl,Br,I]-!@C);!$([F,Cl,Br,I]-!@C-!@[F,Cl,Br,I])]',
                '[N;H0;$(N-c);D2]=[N;D2]=[N;D1]',
                '[N;H0;$(N-C);D2]=[N;D2]=[N;D1]'],
        'FGs': ['Aromatic Acid Chloride',
                'Aliphatic Acid Chloride',
                'Aromatic Carboxylic Acid',
                'Aliphatic Carboxylic Acid',
                'Alpha Amino Acid',
                'Aromatic Sulfonyl Chloride',
                'Aliphatic Sulfonyl Chloride',
                'Aromatic Amine',
                'Aliphatic Amine',
                'Cyclic Amine',
                'Aromatic Boronic Acid',
                'Aliphatic Boronic Acid',
                'Aromatic Isocyanate',
                'Aliphatic Isocyanate',
                'Aromatic Alcohol',
                'Aliphatic Alcohol',
                'Aromatic Aldehyde',
                'Aliphatic Aldehyde',
                'Aromatic Halogen',
                'Aliphatic Halogen',
                'Aromatic Azide',
                'Aliphatic Azide']
    })

    matches = []
    # iterate through each row and select 
    # 'SMARTS' and 'FGs' column respectively.
    for index, row in RDKit_SMARTS_df.iterrows():
        pattern = Chem.MolFromSmarts(row["SMARTS"])
        if mol.HasSubstructMatch(pattern):
           matches.append(row["FGs"])

    #module_logger.debug('calculate_FG - n. of matches: {}'.format(len(matches)))
   
    return matches

def FGFilter(mol_target, df, thsd):
    """ Functional group filter """

    module_logger.info('received a call to "FGFilter"')

    # FGs of target
    target_FGs_set = set(calculate_FG(mol_target))
    n_target_FGs_set = len(target_FGs_set)
    
    # select columns
    df_FG = df.iloc[:, [0, 1]]# raxID, SMILES columns
    
    status = []
    source_FG = [] # FGs of source
    n_common_FGs =[] # number of FGs in common between target and source
    ratio_match = []
    
    for i in df_FG.index:
        smi_source = df_FG.loc[i].iat[1] #SMILES column
        
        # FGs of source
        source_FGs_set = set(calculate_FG(Chem.MolFromSmiles(smi_source)))
        
        if len(source_FGs_set):
            source_FG.append(source_FGs_set)
        else: source_FG.append('')
       
        #operations
        #intersection FGs
        common_FGs_set = target_FGs_set.intersection(source_FGs_set)
        n_common_FGs_set = len(common_FGs_set)
        
        n_common_FGs.append(n_common_FGs_set)
        
        #ratio match
        ratio = 0
        if n_target_FGs_set: 
            ratio = n_common_FGs_set/n_target_FGs_set
        ratio_match.append(ratio)
        
        # status - there exist common functional groups (CFG)?     
        if ratio >= thsd: is_included = 1 # incluso nel dataset filtrato
        else: is_included = 0
        status.append(is_included)
    
    df_FG['Functional groups (FG)'] = source_FG
    df_FG['No. common FGs'] = n_common_FGs
    df_FG['No. target FGs'] = df_FG.apply(lambda x: (n_target_FGs_set), axis = 1)
    df_FG['ratio FG match'] = ratio_match
    df_FG['CFGs?'] = status    
    
    return df_FG

    # target molecule, dataframe
    # dataframe: raxID and SMILES columns
def calculate_structural_similarity(mol_target, df):
    """
    Calculate the structural similarity betweewn a target molecule and the SMILES in the given DataFrame. 
    
    MACCS Key is used as fingerprint and the similarity is calculated using Tanimoto distance.
    
    Parameters
    ----------
    mol_target : RDKit molecule
        A target molecule.
    df : Pandas DataFrame
        A DataFrame containing rax identifiers (raxID) and SMILES in the first and second column, respectively.

    Returns
    -------
    df_str : Pandas DataFrame
        A DataFrame containing information about the structural similarity index for each analogue in the given DataFrame.

    """
    
    module_logger.info('received a call to "calculate_structural_similarity"')

    # fingerprint of the target
    #fp_target = Chem.RDKFingerprint(mol_target) # topological fingerprint
    fp_target = MACCSkeys.GenMACCSKeys(mol_target) # MACCS Key
    
    df_str = df.copy()
    
    similarity_index = []
    for i in df_str.index:
        smi_other = df_str.loc[i].iat[1] # select SMILES column
        fp_mol = MACCSkeys.GenMACCSKeys(Chem.MolFromSmiles(smi_other))
#        fp_mol = Chem.RDKFingerprint(Chem.MolFromSmiles(smi_other))

        # distance between mol and target, default Tanimoto 
#        similarity_index.append( DataStructs.FingerprintSimilarity(fp_target, fp_mol, metric=DataStructs.TanimotoSimilarity) )
        similarity_index.append(DataStructs.FingerprintSimilarity(fp_target, fp_mol))
    
    df_str['Structural Similarity'] = similarity_index

    return df_str

    # target molecule, dataframe
    # dataframe: raxID and SMILES columns
def structuralFilter(mol_target, df_for_filter):
    """
    Filter the given DataFrame of SMILES based on the presence of a structural similarity with respect to the target molecule.
    
    Chemicals in the DataFrame are ranked based on the similarity index.

    Parameters
    ----------
    mol_target : RDKit molecule
        A target molecule.
    df_for_filter : Pandas DataFrame
        A DataFrame containing rax identifiers (raxID) and SMILES in the first and second column, respectively.
 
    Returns
    -------
    df_str_filtered : Pandas DataFrame
        A DataFrame containing the selected and ranked chemicals.

    """
    
    module_logger.info('received a call to "structuralFilter"')
     
    df = calculate_structural_similarity(mol_target, df_for_filter)
    
    # select rows with similarity > 0
    df = df[df['Structural Similarity'] > 0]
   # module_logger.debug('No. rows: %d' % len(df))
    
    # order by 'Structural Similarity' column
    df = df.sort_values(by='Structural Similarity', ascending=False)
    
    # assign a Rank according to 'Structural Similarity' column. 
    # equal values are assigned a rank that is the lowest rank in the group.
    # the rank is a float value
    df['Rank'] = df['Structural Similarity'].rank(method ='min', ascending = 0)
    df['Rank'] = df['Rank'].astype(int)

    return df

def assay_request(smi):
    response = None
    
    try:
            module_logger.debug('SMILES request RAX: %s', smi)
            response =  pcp.request(identifier=smi, namespace='smiles', domain='compound', operation='assaysummary',  output='csv')
    except HTTPError as e:
            module_logger.error(msg = 'HTTPError: ' + str(e.code))
    except URLError as e:
            module_logger.warning(msg = 'URLError: ' + str(e.reason)) #  [Errno 11001] getaddrinfo failed, connection error
#    except httplib.HTTPException as e:
#            module_logger.critical(msg = 'HTTPException')
    except PubChemHTTPError as e:
            module_logger.warning(msg = 'PubChemHTTPError: ' + e.msg)  # PubChemHTTPError: PUGREST.NotFound   
    except Exception:
            import traceback
            module_logger.error(msg = 'Generic exception: ' +  traceback.format_exc())
    else:
            module_logger.debug('Result code: %s', str(response.getcode()))
            response = response.read().rstrip()
    
    return response

    # smi: SMILES string
def get_assays(smi):
    """Assays from the given SMILES using PubChem"""    
    
    filtered_df = None # it could be empty when there are no assays or they are Inconclusive o Unspecified assays
    
    result = assay_request(smi)
    if result is not None: # if no connection problem and no missing assay
        df = pd.read_csv(BytesIO(result))
        df_for_BIO = df[["AID", "Bioactivity Outcome"]]
        
        # group by AID and unique concatenation for bioactivity outcomes
        df_for_BIO = (df_for_BIO.groupby('AID')
            .agg({'Bioactivity Outcome' : lambda x: ', '.join(x.unique())})
            .reset_index())
        
        # when there are more bioactivity outcome for an assay, set inconclusive assays before search for active, probe or inactiva
        mask = df_for_BIO["Bioactivity Outcome"].str.contains(',', regex=False)
        df_for_BIO.loc[mask, 'Bioactivity Outcome'] = "Inconclusive" 
        
        # set activity code
        # if Bioactivity Outcome equal to active or probe then active (1)
        # if Bioactivity Outcome equal to inactive then inactive (0)
        mask = df_for_BIO["Bioactivity Outcome"].str.contains('Active|Probe', regex=True)
        df_for_BIO.loc[mask, 'Bioactivity Outcome'] = "1" 
        mask = df_for_BIO["Bioactivity Outcome"].str.contains('Inactive', regex=False)
        df_for_BIO.loc[mask, 'Bioactivity Outcome'] = "0" 
        
        #select active and inactive assays
        mask = df_for_BIO["Bioactivity Outcome"].str.contains('1|0', regex=True)
        filtered_df = df_for_BIO[mask].reset_index()
        
    else: # if there are connection problem or missing assays
       column_names = ["AID", "Bioactivity Outcome"]
       filtered_df = pd.DataFrame(columns = column_names)

    return filtered_df

def comparison(act_t, act_s): 
    """Function to compare bioactivity assays of target and source"""   
    
    # if target = 0 then, if source = 0 then cn otherwise (source = 1) sp
    if act_t==0:
        if act_s==0: return 'cn' # common negative
        else: return 'sp'  # only source positive 
    # if target = 1 then, if source = 1 then cp otherwise (source = 0) tp
    else: 
        if act_s==1: return 'cp' # common positive
        else: return 'tp' # only target positive
        
# dataframes have to contain columns AID (assay ID) and Bioactivity Outcome
def compare_assays(df_target, df_source): # compare_assays
    
    #counters
    counters_df = pd.DataFrame({'cn': [0], 'cp': [0], 'tp':[0], 'sp':[0]}, index = ['AID'])
    
    if len(df_target) and len(df_source):
    
        # common assays
        df_common_assays = pd.merge(df_target, df_source, on='AID', left_index=False, right_index=False,
                                            indicator=False, validate='one_to_one',suffixes=('_Target', '_Source'))
        
        if len(df_common_assays):
            df_common_assays['Bioactivity Outcome_Target']  = df_common_assays['Bioactivity Outcome_Target'].astype(int)
            df_common_assays['Bioactivity Outcome_Source']  = df_common_assays['Bioactivity Outcome_Source'].astype(int)
        
            # axis 1 or ‘columns’: apply function to each row.
            # error when one of dataframes is empty (i.e., wrong number of items passed 4, placement implies 1)
            df_common_assays['assay_comparison'] = df_common_assays.apply(lambda x : comparison(x['Bioactivity Outcome_Target'], x['Bioactivity Outcome_Source']), axis = 1) 
                            
            # group by assay_comparison
            df_comparison = (df_common_assays.groupby(['assay_comparison'])
                .agg({'AID' : lambda x: x.count()}))
    
            counters_df = counters_df.T
            # Combine using a simple function that chooses the greater column.
            take_greater = lambda s1, s2: s1 if s1.sum() > s2.sum() else s2
            counters_df = counters_df.combine(df_comparison, take_greater, fill_value=0)
            counters_df['AID']  = counters_df['AID'].astype(int)
            counters_df = counters_df.T
    
    return counters_df

    # target SMILES, dataframe
    # dataframe: raxID and SMILES columns
def calculate_biological_similarity(smi_target, df):
    module_logger.info('received a call to "calculate_biological_similarity"')

    df_target_assays = get_assays(smi_target)
    
    # target weight
    # group by bioactivity and count
    bioactivity_target = df_target_assays.groupby('Bioactivity Outcome').size() # series
    # module_logger.debug('calculate_biological_similarity - target - bioactivity outcome size: {}'.format(len(bioactivity_target)))
    bioactivity_target = pd.DataFrame(bioactivity_target) # to dataframe
   
    #counters for number of inactive and active assays in the target
    counters= pd.DataFrame({'0': [0], '1': [0]}).T
    # Combine using a simple function that chooses the greater column.
    take_greater = lambda s1, s2: s1 if s1.sum() > s2.sum() else s2
    counters = counters.combine(bioactivity_target, take_greater, fill_value=0).astype(int)
    counters = counters.T
   
    weight = 0 # initialize weight of target
    # weight = Aa /Ai, where Aa and Ai are number of active and inactive assays in the target
    Ai = counters.at[0, '0'] # inactive
    Aa = counters.at[0, '1'] # active
    module_logger.debug('calculate_biological_similarity - target - No. active and inactive outcomes: {} {}'.format(Aa,Ai))

    if Ai: weight = Aa/Ai
    
    df_bio = df.copy()
    
    cn = []
    cp = []
    tp = []
    sp = []
    
    for i in df_bio.index:
        smi_source = df_bio.loc[i].iat[1] # select SMILES column
        df_source_assays = get_assays(smi_source)    
        df_res = compare_assays(df_target_assays, df_source_assays) # return a dataframe
        cn.append(df_res.at['AID', 'cn'])
        cp.append(df_res.at['AID', 'cp'])
        tp.append(df_res.at['AID', 'tp'])
        sp.append(df_res.at['AID', 'sp'])
               
    df_bio['target weight'] = df_bio.apply(lambda x: (weight), axis = 1)
    df_bio['cn'] = cn
    df_bio['cp'] = cp
    df_bio['tp'] = tp
    df_bio['sp'] = sp

    if len(df_bio):
        # calculate bio_similarity = (cp+cn*weight)/(cp+cn*weight+tp+sp), checking division by zero
        df_bio['Biological Similarity'] = df_bio.apply(lambda x: ( (x['cp'] + x['cn'] * weight) / (x['cp'] + x['cn']*weight +x['tp'] + x['sp']) if (x['cp'] + x['cn']*weight +x['tp'] + x['sp']) > 0 else 0), axis = 1)
        # reliability = ( cp+ cn*weight + tp + sp ) / (Aa + Ai*weight), checking division by zero
        df_bio['Reliability'] = df_bio.apply(lambda x: ( (x['cp'] + x['cn']*weight +x['tp'] + x['sp'] ) / (Aa + Ai*weight) if Aa + Ai*weight > 0 else 0), axis = 1)
        # bio_similarity_weighted =  bio_similarity * reliability
        df_bio['Biological Similarity (weighted with reliability)'] = df_bio.apply(lambda x: (x['Biological Similarity'] * x['Reliability']), axis = 1)
    else: 
        df_bio['Biological Similarity'] = []
        df_bio['Reliability'] = []
        df_bio['Biological Similarity (weighted with reliability)'] = []
    
    return df_bio

    # target SMILES, dataframe
    # dataframe: raxID and SMILES columns
def biologicalFilter(smi_target, df_for_filter):
    """
    Filter the given DataFrame of SMILES based on the presence of a biological similarity with respect to the target.
    
    Chemicals in the DataFrame are ranked based on the similarity index.

    Parameters
    ----------
    smi_target : SMILES string
        A target SMILES.
    df_for_filter : Pandas DataFrame
        A DataFrame containing rax identifiers (raxID) and SMILES in the first and second column, respectively.
 
    Returns
    -------
    df_bio_filtered : Pandas DataFrame
        A DataFrame containing the selected and ranked chemicals.

    """
    module_logger.info('received a call to "biologicalFilter"')
    
    # calculate biological similarity
    df = calculate_biological_similarity(smi_target, df_for_filter)
    #module_logger.debug('No. rows: %d' % len(df))
    
    # select rows with similarity > 0 according to 'Biological Similarity (weighted with reliability)'
    df = df[df['Biological Similarity (weighted with reliability)'] > 0]

    # order by similarity
    df = df.sort_values(by='Biological Similarity (weighted with reliability)', ascending=False)

    # assign a Rank according to 'Biological Similarity (weighted with reliability)' column. 
    # equal values are assigned a rank that is the lowest rank in the group.
    # the rank is a float value
    df['Rank'] = df['Biological Similarity (weighted with reliability)'].rank(method ='min', ascending = 0)
    df['Rank']  = df['Rank'].astype(int)

    return df

def calculate_metabolites(mol):
    """Prediction of metabolites by sygma module"""
 
    # Each step in a scenario lists the ruleset and the number of reaction cycles to be applied
    # SyGMa comes currently with two rulesets:
    # phase1
    #     Phase 1 metabolism rules include mainly different types of oxidation, hydrolysis, reduction and condensation reactions
    # phase2
    #     Phase 2 metabolism rules include severaly conjugation reaction, i.e. with glucuronyl, sulfate, methyl and acetyl 
    
    scenario = sygma.Scenario([
        [sygma.ruleset['phase1'], 1],
        [sygma.ruleset['phase2'], 0]])

    # An rdkit molecule, optionally with 2D coordinates, is required as parent molecule
    parent = mol

    try:
        metabolic_tree = scenario.run(parent)
        metabolic_tree.calc_scores()
        metabolite_list = metabolic_tree.to_list()
        
        metabolites = pd.DataFrame(metabolite_list)
        
    except ZeroDivisionError as err:
        module_logger.debug('calculate_metabolites - handling run-time error: {}'.format(err))
        metabolites = pd.DataFrame({'parent': [parent],
                        'SyGMa_pathway': ['parent;'],
                        'SyGMa_metabolite': [parent],
                        'SyGMa_score': [1.0]})
     
    return metabolites

    # dataframe: raxID and SMILES columns
def calculate_metabolic_similarity(mol_target, df):
    module_logger.info('received a call to "calculate_metaboolic_similarity"')

    metabolites_target = calculate_metabolites(mol_target)
    met_target_df = pd.DataFrame(metabolites_target.iloc[:, [1, 2]]) # select SyGMa_pathway, SyGMa_metabolite
    
    target_pathway_set = set(met_target_df['SyGMa_pathway'])
    target_pathway_set.remove('parent;')
    
    met_target_df['SyGMa_metabolite'] = met_target_df.apply(lambda x : Chem.MolToSmiles(x['SyGMa_metabolite']),axis=1) # metabolites to SMILES
    target_metabolites_set = set(met_target_df['SyGMa_metabolite'])
    
    df_met = df.copy()
    
    similarity_index = []
    
    # pathways
    source_pathways = [] # SyGMa_pathway of source
    common_pathways = []
    number_of_common_pathways = []
    
    target_exclusive_pathways = []
    number_of_target_exclusive_pathways = []
    
    source_exclusive_pathways = []
    number_of_source_exclusive_pathways = []
    
    # metabolites
    common_metabolites = []
    number_of_common_metabolites = []
    
    for i in df_met.index:
        smi = df_met.loc[i].iat[1] # select SMILES column
        mol = Chem.MolFromSmiles(smi) # it can return None, to check
       
        metabolites_mol = calculate_metabolites(mol)
        met_mol_df = pd.DataFrame(metabolites_mol.iloc[:, [1, 2]])# SyGMa_pathway, SyGMa_metabolite
        
        pathway_set = set(met_mol_df['SyGMa_pathway'])
        pathway_set.remove('parent;')
        
        met_mol_df['SyGMa_metabolite'] = met_mol_df.apply(lambda x : Chem.MolToSmiles(x['SyGMa_metabolite']),axis=1) # metabolites to SMILES
        metabolites_set = set(met_mol_df['SyGMa_metabolite'])
        
        #operations
        # SyGMa_pathway of source mol
        if len(pathway_set):
            source_pathways.append(pathway_set)
        else: source_pathways.append('')
        
        #intersection pathways
        common_pathways_set = target_pathway_set.intersection(pathway_set)
        n_common = len(common_pathways_set)
        if n_common:
            common_pathways.append(common_pathways_set)
        else: common_pathways.append('')
        number_of_common_pathways.append(n_common) 
        
        # exclusive pathways of target
        exclusive_pathways_target = target_pathway_set.difference(pathway_set)
        n_exc_target = len(exclusive_pathways_target)
        if n_exc_target:
            target_exclusive_pathways.append(exclusive_pathways_target)
        else: target_exclusive_pathways.append('')   
        number_of_target_exclusive_pathways.append(n_exc_target)

        # exclusive pathways of the current molecule
        exclusive_pathways_source = pathway_set.difference(target_pathway_set)
        n_exc_source = len(exclusive_pathways_source)
        if n_exc_source:
            source_exclusive_pathways.append(exclusive_pathways_source)
        else: source_exclusive_pathways.append('')
        number_of_source_exclusive_pathways.append(n_exc_source)
        
        # calculation similarity index
        index = 0
        denominator = n_common+n_exc_target+n_exc_source
        if denominator:
            index = n_common/denominator
                      
        similarity_index.append(index)
        
        # common metabolites
        common_metabolites_set = target_metabolites_set.intersection(metabolites_set)
        n_common_metabolites = len(common_metabolites_set)
        if n_common_metabolites: common_metabolites.append(common_metabolites_set)
        else: common_metabolites.append('')
        number_of_common_metabolites.append(n_common_metabolites)
        
    # add lists as column in the final dataframe
    df_met['Metabolic pathways'] = source_pathways
    df_met['Common metabolic pathways'] = common_pathways 
    df_met['Target exclusive metabolic pathways'] = target_exclusive_pathways
    df_met['Source exclusive metabolic pathways'] = source_exclusive_pathways

    df_met['No. common metabolic pathways'] = number_of_common_pathways
    df_met['No. target exclusive metabolic pathways'] = number_of_target_exclusive_pathways
    df_met['No. source exclusive metabolic pathways'] = number_of_source_exclusive_pathways
    
    df_met['Common metabolites'] = common_metabolites
    df_met['No. common metabolites'] = number_of_common_metabolites
    
    df_met['Metabolic Similarity'] = similarity_index
   
    return df_met

    # target molecule rdkit, dataframe
    # dataframe: raxID and SMILES columns
def metabolicFilter(mol_target, df_for_filter):
    """
    Filter the given DataFrame of SMILES based on the presence of a metabolic similarity with respect to the target molecule.
    
    Chemicals in the DataFrame are ranked based on the similarity index.

    Parameters
    ----------
    mol_target : RDKit molecule
        A target molecule.
    df_for_filter : Pandas DataFrame
        A DataFrame containing rax identifiers (raxID) and SMILES in the first and second columns, respectively.
        
    Returns
    -------
    df_met_filtered : Pandas DataFrame
        A DataFrame containing the selected and ranked chemicals with information about the metabolic similarity.

    """
    
    module_logger.info('received a call to "metabolicFilter"')
     
    df = calculate_metabolic_similarity(mol_target, df_for_filter)
    #module_logger.debug('No. rows: %d' % len(df))
    
    # select rows with similarity > 0
    df = df[df['Metabolic Similarity'] > 0]
    
    # order by 'Metabolic Similarity' column
    df = df.sort_values(by='Metabolic Similarity', ascending=False)
    
    # assign a Rank according to 'Metabolic Similarity' column. 
    # equal values are assigned a rank that is the lowest rank in the group.
    # the rank is a float value
    df['Rank'] = df['Metabolic Similarity'].rank(method ='min', ascending = 0)
    df['Rank']  = df['Rank'].astype(int)
 
    return df

def merge_lists(filtered_dataset, df_str, df_bio, df_meta):
    
    # common columns (except raxID)
    columns_for_differencing = ['SMILES','Rank']
    
    df_str = df_str[df_str.columns.difference(columns_for_differencing)]
    df_bio = df_bio[df_bio.columns.difference(columns_for_differencing)]
    df_meta = df_meta[df_meta.columns.difference(columns_for_differencing)]
    
    # merge on the common columns (raxID), how = outer
    similarities = pd.merge(filtered_dataset, df_str, on='raxID', how='outer',
                                indicator=False, validate='one_to_one')
    similarities = pd.merge(similarities, df_bio, on='raxID', how='outer',
                                indicator=False, validate='one_to_one')
    similarities = pd.merge(similarities, df_meta, on='raxID', how='outer',
                                indicator=False, validate='one_to_one')
   
    return similarities

def group_by_analogue(df_str, df_bio, df_meta):
    
    tmp_str = df_str.loc[:,['raxID','Rank']]
    tmp_bio = df_bio.loc[:,['raxID','Rank']]
    tmp_meta = df_meta.loc[:,['raxID','Rank']]
    
    # concatenate rows from temporary dataframes
    # axis 0, so the resulting table combines the rows of the input tables
    analogues = pd.concat([tmp_str, tmp_bio, tmp_meta], axis=0)
     
    analogues_groups = (analogues.groupby('raxID')
            .agg({'Rank' : lambda x: ', '.join(x.unique())})
            .reset_index())

    if len(analogues_groups):    
        # count of similarity lists
        analogues_groups['No. groups'] = analogues_groups.apply(lambda x : str(x['Rank']).count(',')+1,axis=1)
        # order by 'No. groups' column
        analogues_groups = analogues_groups.sort_values(by='No. groups', ascending=False)
    else: analogues_groups['No. groups'] = []
        
    return analogues_groups

    # id target, mol target, structural, biological and metabolic similarity list
def fullAnalogues(id_target, mol_target, df_str, df_bio, df_meta, all_similarities):
    module_logger.info('received a call to "fullAnalogues"')
    
    analogues = group_by_analogue(df_str, df_bio, df_meta)
            
    # merge on common columns (raxID), default how = inner
    analogues = pd.merge(analogues, all_similarities, on=None, left_index=False, right_index=False,
                                         indicator=False, validate='one_to_one')
 
    # target info
    # Metabolic pathways
    metabolites_target = calculate_metabolites(mol_target)
   
    target_pathway_set = set(metabolites_target['SyGMa_pathway'])
    target_pathway_set.remove('parent;')
  
    target_pathways = []
    if len(target_pathway_set):
            target_pathways.append(target_pathway_set)
    else: target_pathways.append('')
    
    # FGs of target
    target_FGs_set = set(calculate_FG(mol_target))
    target_FGs = []
    if len(target_FGs_set):
            target_FGs.append(target_FGs_set)
    else: target_FGs.append('')
       
    # concatenation with target info
    target = pd.DataFrame({'raxID': [id_target],
                        'SMILES': [Chem.MolToSmiles(mol_target)],
                        'Metabolic pathways': target_pathways,
                        'Functional groups (FG)': target_FGs,
                        'Rank': ['Target']})
    
    analogues = pd.concat([target, analogues], axis=0)
   
    return analogues

    #id and mol of traget, full list of analogues
def predictions(id_target, mol_target, df_analogues): 
    module_logger.info('received a call to "predictions"')
    
    # controllare caso df_analogues vuoto
    tmp = pd.DataFrame(df_analogues[df_analogues['No. groups'] > 0 ]) # remove target info
    tmp = tmp.loc[:,['No. groups','Activity']]
    
    tmp['Activity (Mode)'] = tmp['Activity'].expanding(1).apply( lambda y: mode(y)[0][0] )
    tmp['Activity (Count)']= tmp.Activity.expanding(1).count()

    #For last value: df.groupby('Column_name').nth(-1)
    df_last_groupby = tmp.groupby('No. groups').nth(-1) 
    # remove column 'Activity'
    df_last_groupby = df_last_groupby.drop(columns=['Activity'])
   
    # counts Groupby value
    df3 = tmp.groupby(['No. groups', 'Activity']).size().unstack(fill_value=0)
    # order by No. group
    df3 = df3.sort_values(by='No. groups', ascending=False)
   
    # Unique elements 
    uniqueValues = tmp['Activity'].unique() # Activity from tmp  
    
    df_expand = pd.DataFrame()
    for i in uniqueValues:
        res = df3[i].expanding(1).apply( lambda y: sum(y) )
        df_expand = pd.concat([df_expand, res], axis=1)
    df_expand = df_expand.set_index(df3.index, 'No. groups')
        
    if len(df_expand):
        df_expand['Activity (Max)'] = df_expand.apply(lambda x : max(x),axis=1)
    else: df_expand['Activity (Max)'] =[]
    
    # As both the dataframes contains similar No. groups on the index. 
    # So, to merge the dataframe on indices pass the left_index & right_index arguments as True
    merged_dataframes = pd.merge(df_last_groupby, df_expand, left_index=True, right_index=True,
                                            indicator=False, validate='one_to_one')
   
    if len(merged_dataframes):
        merged_dataframes['Reliability'] = merged_dataframes.apply(lambda x : x['Activity (Max)']/x['Activity (Count)'] if x['Activity (Count)'] > 0 else 0, axis=1)
    
    else: merged_dataframes['Reliability'] =[0]
    
    #You can change multiple names at once by adding elements to dict.
    merged_dataframes = merged_dataframes.rename(columns={'Activity (Mode)': 'Activity (preds)', 'Activity (Count)': 'cmpds_used_count'})
    
    # add info target as constant column
    # df.assign(new='y')
    merged_dataframes= merged_dataframes.assign(raxID= id_target)
    merged_dataframes= merged_dataframes.assign(SMILES=Chem.MolToSmiles(mol_target))
 
    return merged_dataframes

if __name__ == '__main__':
    print('Util file.')