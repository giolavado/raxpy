# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2021- Giovanna Janet Lavado
#
# This file is part of the RAXpy.
# The contents are covered by the terms of the GNU General Public License
# which is included in the file LICENSE.txt, found at the root
# of the RAXpy source tree.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    
# -----------------------------------------------------------------------------

"""
@author: G.J. Lavado
"""

import tkinter as tk # python 3
import os 
import sys
from pathlib import Path
import logging

from raxModel import Model
from raxView import View

if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
    os.chdir(application_path)

# =============================================================================
# Two loggers. One logger prints only important messages (ERROR and higher) to the console. 
# The second logger records more logging message (DEBUG and higher) in a log file with more details 
# (for example, it includes execution line number or %(lineno)d).
# 
# =============================================================================

# create a logger object instance
logger = logging.getLogger('rax')

# specifies the LOWEST severity for logging
logger.setLevel(logging.DEBUG) 

# set a destination for your logs or a "handler"
# here, we choose to print on console (a consoler handler)
# our first handler is a console handler
console_handler = logging.StreamHandler()
#console_handler.setLevel(logging.WARNING)
console_handler.setLevel(logging.ERROR)
console_handler_format = '%(asctime)s - %(name)s | %(levelname)s: %(message)s'
console_handler.setFormatter(logging.Formatter(console_handler_format))
logger.addHandler(console_handler) # add the console handler to the logger object

# the second handler is a file handler
file_handler = logging.FileHandler('rax.log', mode='a')
file_handler.setLevel(logging.DEBUG)
file_handler_format = '%(asctime)s - %(name)s | %(levelname)s | %(lineno)d: %(message)s'
file_handler.setFormatter(logging.Formatter(file_handler_format))
logger.addHandler(file_handler) # add the file handler to the logger object


class Controller:
    def __init__(self):
        # variables
        
        self.logger = logging.getLogger('rax.Controller')
        self.logger.info('creating an instance of Controller')
                
        self.root = tk.Tk()
        self.WIDTH = 700
        self.HEIGHT = 750
        self.X = 40
        self.Y = 5
        self.model = Model(self)
        self.view = View(self.root, self)
        
        #print(dir(self.view))
        
    #event handlers
    def loadDatasetButtonPressed(self):
        self.logger.info('controller - loadDatasetButtonPressed')
        self.logger.debug('controller - dataset %s', self.view.var_dataset.get())
        self.model.load_dataset(self.view.var_dataset.get())

    def runButtonPressed(self):
        self.logger.info('controller - runButtonPressed')
        self.model.calculate(self.view.entered_id_target.get(), self.view.entered_smi_target.get().strip(),
                             self.view.entered_n_str, self.view.entered_n_bio, self.view.entered_n_meta,
                             self.view.var_filter_mcs.get(), self.view.entered_thd_mcs,
                             self.view.var_filter_fg.get(), self.view.entered_thd_fg)
        
    def saveButtonPressed(self,path):
        self.logger.info('controller - saveButtonPressed')
        self.model.save_results(path)
        
    def datasetChangedDelegate(self):
        self.logger.info('controller - datasetChangedDelegate')
 
        # model internally chages and needs to signal a change
        self.view.update_txt_box(self.model.myLOG)
        
        if self.model.is_loaded_dataset: 
            self.view.btn_run['state'] = tk.NORMAL
        else: self.view.btn_run['state'] = tk.DISABLED

    def calculationRaxChangedDelegate(self):
        self.logger.info('controller - calculationRaxChangedDelegate')

        #model internally chages and needs to signal a change
        self.view.update_txt_box(self.model.myLOG)
        
        if self.model.is_calculated: 
            self.view.btn_save['state'] = tk.NORMAL
        else: self.view.btn_save['state'] = tk.DISABLED
    
    def saveResultsChangedDelegate(self):
        self.logger.info('controller - saveResultsChangedDelegate')
 
        # model internally chages and needs to signal a change
        self.view.update_txt_box(self.model.myLOG)
        
    def run(self):
        self.logger.info('controller - run')
        
#        self.logger.debug('application path: {}'.format(Path.cwd()))
#        self.logger.info('OS name: {}, platform: {}'.format(os.name,sys.platform))
     
        # img_folder = Path.cwd() / "images"
        
        # if sys.platform.startswith('win'):
        #     ico_path = img_folder / 'rax.ico'
        #     self.root.iconbitmap(ico_path)
        # else:
        #     ico_path = img_folder / 'rax.png'
        #     logo = tk.PhotoImage(file=ico_path)
        #     self.root.call('wm', 'iconphoto', self.root._w, logo)
        
        # Sizing and Positioning
        
#        self.root.geometry("%sx%s" % (self.WIDTH, self.HEIGHT))
        self.root.geometry("%sx%s+%s+%s" % (self.WIDTH, self.HEIGHT, self.X, self.Y))
        self.root.minsize(self.WIDTH, self.HEIGHT)
        self.root.title("RAXpy")
        
        self.root.deiconify()
        self.root.mainloop()